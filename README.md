# SPL4J

![Version](https://img.shields.io/badge/dynamic/json?color=informational&label=SW+version&query=%24.general.version.sw&url=https%3A%2F%2Fcodeberg.org%2Fatlas144%2Fspl4j%2Fraw%2Fbranch%2Fmain%2Fshields.json)
[![License](https://img.shields.io/badge/dynamic/json?color=green&label=SW+license&query=%24.general.license.sw&url=https%3A%2F%2Fcodeberg.org%2Fatlas144%2Fspl4j%2Fraw%2Fbranch%2Fmain%2Fshields.json)](https://codeberg.org/atlas144/spl4j/src/branch/main/SW_LICENSE)
![Language](https://img.shields.io/badge/dynamic/json?color=red&label=language&query=%24.general.language&url=https%3A%2F%2Fcodeberg.org%2Fatlas144%2Fspl4j%2Fraw%2Fbranch%2Fmain%2Fshields.json)
![JDK version](https://img.shields.io/badge/dynamic/json?color=informational&label=JDK&query=%24.specific.jdk&url=https%3A%2F%2Fcodeberg.org%2Fatlas144%2Fspl4j%2Fraw%2Fbranch%2Fmain%2Fshields.json)

[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)

## Description

SPL4J is a simple tool which lists information about serial ports of the device, which are currently used (connected to other device).

## Usage

Just download the latest [release](https://codeberg.org/atlas144/spl4j/releases) `.jar` and run (from the folder where the `.jar` was downloaded):

```bash
java -jar spl4j.jar
```

## Contributing

Contributions are welcome!

## License

The source code is licensed under the [MIT](https://codeberg.org/atlas144/spl4j/src/branch/main/LICENSE) license.
