// SPDX-License-Identifier: MIT
package page.codeberg.atlas144.spl4j;

import com.fazecast.jSerialComm.SerialPort;

/**
 *
 * @author atlas144
 */
public class Spl4j {

    public static void main(String[] args) {
        SerialPort[] ports = SerialPort.getCommPorts();
        
        System.out.println("---------------------");
        
        if (ports.length > 0) {
            for (SerialPort port : ports) {
                System.out.printf(
                        "Name: %s\nSystem name: %s\nDescription: %s\nPath: %s\nPhysical location: %s",
                        port.getDescriptivePortName(),
                        port.getSystemPortName(),
                        port.getPortDescription(),
                        port.getSystemPortPath(),
                        port.getPortLocation()
                );
            }
        } else {
            System.out.print("No used ports found.");
        }
        
        System.out.println("\n---------------------");
    }
}
